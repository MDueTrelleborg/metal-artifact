#ifndef INCLUDE_METAL_SHADER_TYPES
#define INCLUDE_METAL_SHADER_TYPES

#include <metal_stdlib>
#include <simd/simd.h>

typedef vector_float4 v4;
typedef vector_float3 v3;
typedef vector_float2 v2;
typedef vector_int3 v3i;
typedef vector_int2 v2i;
typedef float f32;
typedef int s32;
typedef unsigned int u32;

typedef matrix_float2x2 m2x2;
typedef matrix_float3x3 m3x3;
typedef matrix_float3x3 m4x4;

#define M2x2 m2x2
#define M3x3 m3x3
#define M4x4 m4x4

#define V2 v2
#define V3 v3
#define V4 v4

#define vec2 v2
#define vec3 v3
#define vec4 v4

#define V2i v2i
#define V3i v3i

#define Pi32 3.14159265359f

struct VertInputF4F4
{
    v4 position [[attribute(0)]];
    v4 color    [[attribute(1)]];
};

struct VertInputF4F4F2
{
    v4 position [[attribute(0)]];
    v4 color    [[attribute(1)]];
    v2 uv       [[attribute(2)]];
};

struct SDFVertInput
{
    v4 position [[attribute(0)]];
    v2 a    [[attribute(1)]];
    v2 b    [[attribute(2)]];
    v4 colorThickness    [[attribute(3)]];
};

struct VertOutputF4F4
{
    v4 position [[position]];
    v4 color;
};

struct VertOutputF4F4F2
{
    v4 position [[position]];
    v4 color;
    v2 uv;
};

struct SDFVertOutput
{
    v4 position [[position]];
    v4 color;
    v2 a;
    v2 b;
};

#endif // INCLUDE_METAL_SHADER_TYPES
