vertex SDFVertOutput
SDFTextureVertexMain(SDFVertInput input [[stage_in]],
                     constant matrix_float4x4 *viewProjection [[buffer(1)]])
{
    SDFVertOutput result;

    result.position = *viewProjection * V4(input.position.x, input.position.y, 0.0, 1.0);
    result.color = input.colorThickness;
    result.a = input.a;
    result.b = input.b;
    return result;
}

fragment float4
SDFTextureFragmentMain(SDFVertOutput input [[stage_in]], constant f32 *timer [[buffer(1)]], constant v2i *viewSize [[buffer(2)]])
{
    v2 resolution = V2(viewSize->x, viewSize->y);
    
    v2 uvPixels = input.position.xy;
    v2 p = (-resolution + 2.0 * uvPixels) / resolution;
    p.y *= -resolution.y / resolution.x;

    f32 s = input.color.w / resolution.y;
    f32 falloff = 2.0 / resolution.y;
    
    v4 line1 = V4(1.0);
    v2 a = input.a; 
    v2 b = input.b; 

    f32 d = sdLine(p, a, b);

    // @todo: Learn how to adjust the levels of the thing.
    f32 alpha = 1.0 - smoothstep(s - falloff, s + falloff, abs(d));
    // f32 alpha = 1.0 - smoothstep(0, s, d);
    alpha = clamp(alpha, 0.0, 1.0);

    line1.rgb = input.color.rgb * alpha;
    line1.a = alpha;

// #define DEBUGGING
#ifdef DEBUGGING
    return line1 + V4(0.2, 0.2, 0.2, 0.5);
#else
    return line1;
#endif
}
