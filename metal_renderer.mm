@implementation MetalRenderer
{
    f32 _desiredAspectRatio; // x / y
    s32 _maxSampleCount;

    id <MTLDevice> _device;
    id <MTLCommandQueue> _commandQueue;
    
    CAMetalLayer *_metalLayer;
    
    u32 _shaderCount;
    TRMetalShader *_shaders[32];

    texture_info _renderTexture;
    id<MTLTexture> _textures[MAX_TEXTURE_COUNT];

    texture_info _stencilTexture;
    texture_info _depthTexture;
    texture_info _msaaDepthTexture;
    texture_info _msaaTexture;
    
    id<MTLSamplerState> _textureSamplerLinear;
    id<MTLSamplerState> _textureSamplerNearest;
    
    id<MTLDepthStencilState> _depthState;
    id<MTLDepthStencilState> _stencilState;
    id<MTLDepthStencilState> _nextStencilState;
    id<MTLDepthStencilState> _zeroDepthStencilState;
    
    uint8_t _flightBufferIndex;
    dispatch_semaphore_t _inFlightSemaphore;
    
    id<MTLBuffer> _simpleVertexBuffer[GlobalMaxBuffersInFlight];
    id<MTLBuffer> _simpleIndexBuffer[GlobalMaxBuffersInFlight];
    id<MTLBuffer> _textureVertexBuffer[GlobalMaxBuffersInFlight];
    id<MTLBuffer> _textureIndexBuffer[GlobalMaxBuffersInFlight];
    id<MTLBuffer> _sdfVertexBuffer[GlobalMaxBuffersInFlight];
    id<MTLBuffer> _sdfIndexBuffer[GlobalMaxBuffersInFlight];

    MTLRenderPassDescriptor *_primitiveDescriptor;
    MTLRenderPassDescriptor *_singleShaderDescriptor;
    MTLRenderPassDescriptor *_offscreenToBackbuffer;
}

v2i V2iFromCGSize(CGSize size) {
    return V2i((s32)size.width, (s32)size.height);
}

CGSize MakeCGSize(v2i size) {
    return CGSizeMake(size.x, size.y);
}

// @todo: Why do we have a desired aspect ratio, when we also have the screen size ?
- (instancetype)init:(CAMetalLayer *)metalLayer desiredAspectRatio:(f32)desiredAspectRatio
{
    if(self = [super init])
    {
        _metalLayer = metalLayer;
        _device = _metalLayer.device;
        _commandQueue = [_device newCommandQueue];
        assert(_device != nil);
        assert(_commandQueue != nil);
        _inFlightSemaphore = dispatch_semaphore_create(GlobalMaxBuffersInFlight);
        _maxSampleCount = [self getMaxSampleCount:_device];
        _desiredAspectRatio = desiredAspectRatio;
        _shaderCount = 0;
        _backbufferTexture = TextureInfo(0, V2iFromCGSize(_metalLayer.drawableSize));
     
        u32 count = U16Max;
        tr_fiz(GlobalMaxBuffersInFlight) {  // @todo: Is this the way to do buffers that are shared to the gpu?
            _simpleVertexBuffer[i] = [_device newBufferWithLength:sizeof(simple_vertex) * count options:MTLResourceStorageModeShared];
            _simpleIndexBuffer[i] = [_device newBufferWithLength:sizeof(u16) * count options:MTLResourceStorageModeShared];
            _textureVertexBuffer[i] = [_device newBufferWithLength:sizeof(textured_vertex) * count options:MTLResourceStorageModeShared];
            _textureIndexBuffer[i] = [_device newBufferWithLength:sizeof(u16) * count options:MTLResourceStorageModeShared];
            _sdfVertexBuffer[i] = [_device newBufferWithLength:sizeof(sdf_vertex) * count options:MTLResourceStorageModeShared];
            _sdfIndexBuffer[i] = [_device newBufferWithLength:sizeof(u16) * count options:MTLResourceStorageModeShared];
        }

        [self createStaticTextures];
        [self createTextureSamplers];
        
        [self createShaders];
        [self createDepthStencilStates];

        // Create renderpass descriptors
        _singleShaderDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
        _singleShaderDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
        _singleShaderDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
        _singleShaderDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.0f, 0.0f, 0.0f, 1.0f);
        
        _primitiveDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
        _primitiveDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
        _primitiveDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.0f, 0.0f, 0.0f, 1.0f);
        _primitiveDescriptor.colorAttachments[0].storeAction = MTLStoreActionStoreAndMultisampleResolve;
        // _primitiveDescriptor.depthAttachment.clearDepth = 1.0f;
        // _primitiveDescriptor.depthAttachment.depthResolveFilter = MTLMultisampleDepthResolveFilterSample0; // default
        // _primitiveDescriptor.depthAttachment.loadAction = MTLLoadActionClear;

        _offscreenToBackbuffer = [MTLRenderPassDescriptor renderPassDescriptor];
        _offscreenToBackbuffer.colorAttachments[0].storeAction = MTLStoreActionStore;
        _offscreenToBackbuffer.colorAttachments[0].loadAction = MTLLoadActionClear;
        _offscreenToBackbuffer.colorAttachments[0].clearColor = MTLClearColorMake(0.0f, 0.0f, 0.0f, 1.0f);
    } else {
        InvalidCodePath;
    }
    
    return self;
}

- (u32)getMaxSampleCount:(id<MTLDevice>)device
{
    u32 result = 1;
    
    u32 commonSampleCounts[3] = { 2, 4, 8 };

    tr_fiz(ArrayCount(commonSampleCounts)) {
        u32 sampleCount = commonSampleCounts[i];
        if([_device supportsTextureSampleCount:sampleCount]) {
            result = sampleCount;
        }
    }
    
    return result;
}

- (TRMetalShader *)getShader:(shader_type)type
{
    TRMetalShader *result = nil;

    tr_fiz(_shaderCount) {
        TRMetalShader *shader = _shaders[i];
        if(shader.type == type) {
            result = shader;
            break;
        }
    }
    
    assert(result != nil);
    return result;
}

- (void)createTextureSamplers
{
    {
        MTLSamplerDescriptor *samplerDescriptor = [MTLSamplerDescriptor new];
        samplerDescriptor.minFilter = MTLSamplerMinMagFilterLinear;
        samplerDescriptor.magFilter = MTLSamplerMinMagFilterLinear;
        samplerDescriptor.sAddressMode = MTLSamplerAddressModeClampToEdge;
        samplerDescriptor.tAddressMode = MTLSamplerAddressModeClampToEdge;
        
        _textureSamplerLinear = [_device newSamplerStateWithDescriptor:samplerDescriptor];
        assert(_textureSamplerLinear != nil);
    }
    
    {
        MTLSamplerDescriptor *samplerDescriptor = [MTLSamplerDescriptor new];
        samplerDescriptor.minFilter = MTLSamplerMinMagFilterNearest;
        samplerDescriptor.magFilter = MTLSamplerMinMagFilterNearest;
        samplerDescriptor.sAddressMode = MTLSamplerAddressModeClampToEdge;
        samplerDescriptor.tAddressMode = MTLSamplerAddressModeClampToEdge;
        
        _textureSamplerNearest = [_device newSamplerStateWithDescriptor:samplerDescriptor];
        assert(_textureSamplerNearest != nil);
    }
}

- (void)resetRenderTextures {
    tr_fiz(ArrayCount(_textures)) {
        _textures[i] = nil;
    }
    [self createStaticTextures];
}

- (s32)getFreeTextureHandle {

    s32 result = -1;
    tr_fiz(ArrayCount(_textures)) {
        if(_textures[i] == nil) {
            result = i;
            break;
        }
    }
    
    Assert(result != -1);

    return result;
}

- (id<MTLTexture>)getTexture:(s32)textureHandle {
    Assert(textureHandle >= 0 && textureHandle < ArrayCount(_textures));
    id<MTLTexture> result = _textures[textureHandle];
    return result;
}

- (void)deallocTexture:(s32)textureHandle {
    if(textureHandle >= 0 && textureHandle < ArrayCount(_textures)) {
        _textures[textureHandle] = nil;
    }
}

v2i CalculateRenderSize(v2i backbufferSize, f32 desiredAspectRatio) {
    f32 backbufferRatio = (f32)backbufferSize.x / (f32)backbufferSize.y;
    if(desiredAspectRatio <= 0.f) {
        InvalidCodePath;
    }

    v2i renderSize = backbufferSize;
    if(backbufferRatio < desiredAspectRatio) { // Use black bars top
        renderSize.y = (s32) floorf((f32)backbufferSize.x / desiredAspectRatio);
    } else if(backbufferRatio > desiredAspectRatio) { // Use black bars sides
        renderSize.x = (s32) floorf((f32)backbufferSize.y * desiredAspectRatio);
    } else {
        // Do nothing, ratios fit.
    }
    
    return renderSize;
}

- (void)createStaticTextures
{
    [self deallocTexture:_renderTexture.renderHandle];
    [self deallocTexture:_offscreenTextures[0].renderHandle];
    [self deallocTexture:_offscreenTextures[1].renderHandle];
    [self deallocTexture:_offscreenTextures[2].renderHandle];
    [self deallocTexture:_depthTexture.renderHandle];
    [self deallocTexture:_msaaDepthTexture.renderHandle];
    [self deallocTexture:_stencilTexture.renderHandle];
    [self deallocTexture:_msaaTexture.renderHandle];
    
    v2i drawableSize = V2iFromCGSize(_metalLayer.drawableSize);
    f32 aspectRatio = (f32)drawableSize.x / (f32)drawableSize.y;
    v2i renderSize = CalculateRenderSize(drawableSize, aspectRatio);
    
    _offscreenTextures[0] = [self createTexture:TextureType::OFFSCREEN_TARGET size:renderSize sampleCount:1];
    _offscreenTextures[1] = [self createTexture:TextureType::OFFSCREEN_TARGET size:renderSize sampleCount:1];
    _offscreenTextures[2] = [self createTexture:TextureType::OFFSCREEN_TARGET size:renderSize sampleCount:1];
    _renderTexture    = [self createTexture:TextureType::RENDER_TARGET size:renderSize sampleCount:1];
    _depthTexture     = [self createTexture:TextureType::DEPTH size:renderSize sampleCount:1];
    _msaaDepthTexture = [self createTexture:TextureType::DEPTH_MSAA size:renderSize sampleCount:_maxSampleCount];
    _stencilTexture   = [self createTexture:TextureType::STENCIL size:renderSize sampleCount:_maxSampleCount];
    _msaaTexture      = [self createTexture:TextureType::MSAA size:renderSize sampleCount:_maxSampleCount];
}

- (texture_info)makeTexture:(MTLTextureDescriptor *)desc {
    s32 textureHandle = [self getFreeTextureHandle];
    _textures[textureHandle] = [_device newTextureWithDescriptor:desc];

    v2i size = V2i((s32)desc.width, (s32)desc.height);
    texture_info result = TextureInfo(textureHandle, size);
    return result;
}

- (texture_info)createTexture:(TextureType)type size:(v2i)size sampleCount:(u32)sampleCount
{
    texture_info result = {};

    MTLTextureDescriptor *desc = [[MTLTextureDescriptor alloc] init];   

    b32 multiSampling = sampleCount > 1;

    switch(type) {
        case TextureType::OFFSCREEN_TARGET: {
            desc.textureType = MTLTextureType2D;
            desc.pixelFormat = MTLPixelFormatBGRA8Unorm;
        } break;

        case TextureType::RENDER_TARGET: {
            desc.textureType = MTLTextureType2D;
            desc.pixelFormat = MTLPixelFormatBGRA8Unorm;
        } break;
    
        case TextureType::STANDARD: { 
            desc.textureType = MTLTextureType2D; 
            desc.pixelFormat = MTLPixelFormatBGRA8Unorm;
        } break;
        
        case TextureType::MSAA: { 
            desc.textureType = multiSampling ? MTLTextureType2DMultisample : MTLTextureType2D; 
            desc.pixelFormat = MTLPixelFormatBGRA8Unorm;
        } break;
        
        case TextureType::STENCIL: { 
            desc.textureType = multiSampling ? MTLTextureType2DMultisample : MTLTextureType2D; 
            desc.pixelFormat = MTLPixelFormatStencil8;
        } break;

        case TextureType::DEPTH: { 
            desc.textureType = MTLTextureType2D;
            desc.pixelFormat = MTLPixelFormatDepth32Float;
        } break;
        
        case TextureType::DEPTH_MSAA: {
            desc.textureType = multiSampling ? MTLTextureType2DMultisample : MTLTextureType2D;
            desc.pixelFormat = MTLPixelFormatDepth32Float;
        } break;
        
        InvalidDefaultCase;
    }

    desc.width = size.x;
    desc.height = size.y;
    desc.sampleCount = sampleCount;
    desc.mipmapLevelCount = 1;
    desc.storageMode = MTLStorageModePrivate; // The resource is stored in memory only accessible to the GPU, on IOS this is system memory, on macOS this is video memory.
    desc.usage = MTLTextureUsageRenderTarget;
    
    if(type == TextureType::OFFSCREEN_TARGET || type == TextureType::RENDER_TARGET) {
        desc.usage = MTLTextureUsageRenderTarget | MTLTextureUsageShaderRead;
    } 

    result = [self makeTexture:desc];
    return result;
}

- (texture_info)createTexture:(u8 *)data size:(v2i)size bytesPerPixel:(u32)bytesPerPixel format:(pixel_format)format
{
    texture_info result = {};

    MTLTextureDescriptor *desc = [[MTLTextureDescriptor alloc] init];
    desc.width = size.x;
    desc.height = size.y;
    
    if(format == PixelFormat_RGBA) {
        desc.pixelFormat = MTLPixelFormatRGBA8Unorm;
    } else if (format == PixelFormat_BGRA) {
        desc.pixelFormat = MTLPixelFormatBGRA8Unorm;
    } else {
        InvalidCodePath;
    }
    
    desc.textureType = MTLTextureType2D;
    
    result = [self makeTexture:desc];
    
    MTLRegion region = MTLRegionMake2D(0, 0, size.x, size.y);
    NSUInteger bytesPerRow = bytesPerPixel * size.x;
    [[self getTexture:result.renderHandle] replaceRegion:region mipmapLevel:0 withBytes:data bytesPerRow:bytesPerRow];

    return result;
}

- (void)createDepthStencilStates
{
    {
        MTLDepthStencilDescriptor *desc = [[MTLDepthStencilDescriptor alloc] init];
        desc.depthWriteEnabled = NO;
        desc.frontFaceStencil = nil;
        desc.backFaceStencil = nil;
        _zeroDepthStencilState = [_device newDepthStencilStateWithDescriptor:desc];
    }
    
    {
        MTLDepthStencilDescriptor *desc = [[MTLDepthStencilDescriptor alloc] init];
        desc.depthWriteEnabled = YES;
        desc.depthCompareFunction = MTLCompareFunctionLessEqual;
        desc.frontFaceStencil = nil;
        desc.backFaceStencil = nil;
        _depthState = [_device newDepthStencilStateWithDescriptor:desc];
    }
    
    {
        MTLDepthStencilDescriptor *desc = [[MTLDepthStencilDescriptor alloc] init];
        desc.depthWriteEnabled = NO;
        desc.frontFaceStencil.stencilCompareFunction = MTLCompareFunctionAlways;
        desc.frontFaceStencil.stencilFailureOperation = MTLStencilOperationKeep;
        desc.frontFaceStencil.depthFailureOperation = MTLStencilOperationKeep;
        desc.frontFaceStencil.depthStencilPassOperation = MTLStencilOperationReplace;
        desc.frontFaceStencil.writeMask = 0xFF;
        desc.frontFaceStencil.readMask = 0xFF;
        desc.backFaceStencil = nil;
        _stencilState = [_device newDepthStencilStateWithDescriptor:desc];
    }
    
    {
        MTLDepthStencilDescriptor *desc = [[MTLDepthStencilDescriptor alloc] init];
        desc.depthWriteEnabled = NO;
        desc.frontFaceStencil.stencilCompareFunction = MTLCompareFunctionEqual;
        desc.frontFaceStencil.stencilFailureOperation = MTLStencilOperationKeep;
        desc.frontFaceStencil.depthFailureOperation = MTLStencilOperationKeep;
        desc.frontFaceStencil.depthStencilPassOperation = MTLStencilOperationReplace;
        desc.frontFaceStencil.writeMask = 0x00;
        desc.frontFaceStencil.readMask = 0xFF;
        desc.backFaceStencil = nil;
        _nextStencilState = [_device newDepthStencilStateWithDescriptor:desc];
    }
}

- (void)premulBlend:(MTLRenderPipelineColorAttachmentDescriptor *)attachment
{
    attachment.pixelFormat = MTLPixelFormatBGRA8Unorm;
    attachment.blendingEnabled = YES;
    attachment.rgbBlendOperation           = MTLBlendOperationAdd;
    attachment.alphaBlendOperation         = MTLBlendOperationAdd;
    attachment.sourceRGBBlendFactor        = MTLBlendFactorOne;
    attachment.sourceAlphaBlendFactor      = MTLBlendFactorOne;
    attachment.destinationRGBBlendFactor   = MTLBlendFactorOneMinusSourceAlpha;
    attachment.destinationAlphaBlendFactor = MTLBlendFactorOneMinusSourceAlpha;
}

- (void)standardColorAttachment:(MTLRenderPipelineColorAttachmentDescriptor *)attachment
{
    attachment.pixelFormat = MTLPixelFormatBGRA8Unorm;
    attachment.blendingEnabled = YES;
    attachment.rgbBlendOperation           = MTLBlendOperationAdd;
    attachment.alphaBlendOperation         = MTLBlendOperationAdd;
    attachment.sourceRGBBlendFactor        = MTLBlendFactorSourceAlpha;
    attachment.sourceAlphaBlendFactor      = MTLBlendFactorSourceAlpha;
    attachment.destinationRGBBlendFactor   = MTLBlendFactorOneMinusSourceAlpha;
    attachment.destinationAlphaBlendFactor = MTLBlendFactorOneMinusSourceAlpha;
}

- (id<MTLLibrary>)loadShaderLibrary:(char *)shaderCode success:(s32 *)outSuccess
{
    NSString *source = [NSString stringWithCString:shaderCode encoding:NSASCIIStringEncoding];
    
    MTLCompileOptions *compileOptions = [MTLCompileOptions alloc];
    compileOptions.languageVersion = MTLLanguageVersion2_0;
    compileOptions.fastMathEnabled = YES;
    
    __autoreleasing NSError *error = nil;
    id<MTLLibrary> lib = [_device newLibraryWithSource:source options:compileOptions error:&error];
    
    b32 success = true;
    if (error != nil) {
        if (error.code == MTLLibraryErrorCompileWarning) {
            printf("Shader compiled with warnings.\n");
            printf("%s\n\n", [error.localizedDescription UTF8String]);
        } else if (error.code == MTLLibraryErrorCompileFailure) {
            printf("\nFailed to compile shaders: %s\n\n", [error.localizedDescription UTF8String]);
            success = false;
        }
    }

    if (success) {
        *outSuccess = 1;
    } else {
        *outSuccess = -1;
    }
    
    return lib;
}

- (TRMetalShader *)TRMetalShader:(shader_type)type
{
    TRMetalShader *result = [[TRMetalShader alloc] init];
    result.type = type;
    return result;
}

- (void)createShaders
{
    if (_shaderCount == 0) {
        _shaders[_shaderCount++] = [self TRMetalShader:ShaderType_Simple];
        _shaders[_shaderCount++] = [self TRMetalShader:ShaderType_Texture];
        _shaders[_shaderCount++] = [self TRMetalShader:ShaderType_RGBATexture];
        _shaders[_shaderCount++] = [self TRMetalShader:ShaderType_SDF];
        _shaders[_shaderCount++] = [self TRMetalShader:ShaderType_Single];
        _shaders[_shaderCount++] = [self TRMetalShader:ShaderType_TextureCombiner];
    }

    __autoreleasing NSError *error = nil;
    
#if 0
    id<MTLLibrary> library = [_device newDefaultLibrary];
#else
    printf("Loading Shaders\n");
    
    // @todo: Make this relative somehow.    
    char *folderPath = "/Users/due/Projects/Slideshow/Slideshow/MacShared/shaders/";

    // @todo: When I add a shader it's just manual labor.
    const char *filesToLoad[] = {
        "metal_shader_types.h",
        "metal_math.metal",
        "simple_shader.metal",
        "sdf_shader.metal",
        "single.metal",
        "texture_shader.metal",
        "rgba_texture.metal",
        "texture_combiner.metal",
    };
    const u32 fileCount = ArrayCount(filesToLoad);
    
    file_content files[fileCount];

    char *runtimeCompilation = "#define RUNTIME_COMPILATION";
    char *usingMetal         = "using namespace metal;";

    u64 dataSize = StringLength(runtimeCompilation) + StringLength(usingMetal);
    char buf[256];
    tr_fiz(fileCount) {
        sprintf(buf, "%s%s", folderPath, filesToLoad[i]);
        files[i] = ReadEntireFileAndNullTerminate(buf);
        assert(files[i].size != 0);
        dataSize += files[i].size;
    }
    
    u32 length = 0;
    char *fullShaderCode = (char *)malloc(sizeof(char) * dataSize);
    length += sprintf(fullShaderCode, "%s\n%s\n", runtimeCompilation, usingMetal);
    tr_fiz(fileCount) {
        length += sprintf(fullShaderCode + length, "%s\n", (char *)files[i].data);
    }
    Assert(length == dataSize + 2); // Right now there is 2 extra \n characters.
    
    NSString *source = [NSString stringWithCString:fullShaderCode encoding:NSASCIIStringEncoding];
    
    s32 success;
    id<MTLLibrary> library = [self loadShaderLibrary:fullShaderCode success:&success];
    free(fullShaderCode);
    tr_fiz(fileCount) {
        free(files[i].data);
    }

    if(success == -1) {
        printf("Could not compile shaders!\n");
        return;
    }

#endif
    
    {   // Simple pipeline
        MTLRenderPipelineDescriptor *desc = [MTLRenderPipelineDescriptor new];
        
        desc.vertexFunction = [library newFunctionWithName:@"SimpleVertexMain"];
        desc.fragmentFunction = [library newFunctionWithName:@"SimpleFragmentMain"];
        assert(desc.vertexFunction != nil);
        assert(desc.fragmentFunction != nil);
        
        MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
        vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.attributes[0].offset = 0;

        vertexDescriptor.attributes[1].format = MTLVertexFormatUChar4Normalized;
        vertexDescriptor.attributes[1].bufferIndex = 0;
        vertexDescriptor.attributes[1].offset = sizeof(float) * 4;
        
        vertexDescriptor.layouts[0].stride = sizeof(float) * 4 + sizeof(u32);
        vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
    
        desc.vertexDescriptor = vertexDescriptor;
        // desc.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float;
        // desc.stencilAttachmentPixelFormat = MTLPixelFormatStencil8;
        desc.sampleCount = _maxSampleCount;
        [self premulBlend:desc.colorAttachments[0]];
        
        TRMetalShader *shader = [self getShader:ShaderType_Simple];
        shader.pipeline = [_device newRenderPipelineStateWithDescriptor:desc error:&error];
        assert(shader.pipeline);
    }
    
    { // Texture pipeline
        MTLRenderPipelineDescriptor *desc = [MTLRenderPipelineDescriptor new];
        
        desc.vertexFunction = [library newFunctionWithName:@"RGBATextureVertexMain"];
        desc.fragmentFunction = [library newFunctionWithName:@"RGBATextureFragmentMain"];
        assert(desc.vertexFunction != nil);
        assert(desc.fragmentFunction != nil);
        
        MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
        vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.attributes[0].offset = 0;

        vertexDescriptor.attributes[1].format = MTLVertexFormatFloat4;
        vertexDescriptor.attributes[1].bufferIndex = 0;
        vertexDescriptor.attributes[1].offset = sizeof(float) * 4;
        
        vertexDescriptor.layouts[0].stride = sizeof(float) * 4 + sizeof(float) * 4;
        vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
    
        desc.vertexDescriptor = vertexDescriptor;
        desc.sampleCount = 1;
        [self premulBlend:desc.colorAttachments[0]];

        TRMetalShader *shader = [self getShader:ShaderType_RGBATexture];
        shader.pipeline = [_device newRenderPipelineStateWithDescriptor:desc error:&error];
        assert(shader.pipeline);
    }
    
     { // Texture pipeline
        MTLRenderPipelineDescriptor *desc = [MTLRenderPipelineDescriptor new];
        
        desc.vertexFunction = [library newFunctionWithName:@"TextureVertexMain"];
        desc.fragmentFunction = [library newFunctionWithName:@"TextureFragmentMain"];
        assert(desc.vertexFunction != nil);
        assert(desc.fragmentFunction != nil);
        
        MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
        vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.attributes[0].offset = 0;

        vertexDescriptor.attributes[1].format = MTLVertexFormatUChar4Normalized;
        vertexDescriptor.attributes[1].bufferIndex = 0;
        vertexDescriptor.attributes[1].offset = sizeof(float) * 4;
        
        vertexDescriptor.attributes[2].format = MTLVertexFormatFloat2;
        vertexDescriptor.attributes[2].bufferIndex = 0;
        vertexDescriptor.attributes[2].offset = sizeof(float) * 4 + sizeof(u32);
        
        vertexDescriptor.layouts[0].stride = sizeof(float) * 4 + sizeof(u32) + sizeof(float) * 2; // @todo: Can I automatize this?
        vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
    
        desc.vertexDescriptor = vertexDescriptor;
        desc.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float;
        // desc.stencilAttachmentPixelFormat = MTLPixelFormatStencil8;
        desc.sampleCount = _maxSampleCount;
        [self premulBlend:desc.colorAttachments[0]];

        TRMetalShader *shader = [self getShader:ShaderType_Texture];
        shader.pipeline = [_device newRenderPipelineStateWithDescriptor:desc error:&error];
        assert(shader.pipeline);
    }
    
    { // SDF pipeline
        MTLRenderPipelineDescriptor *desc = [MTLRenderPipelineDescriptor new];
        
        desc.vertexFunction = [library newFunctionWithName:@"SDFTextureVertexMain"];
        desc.fragmentFunction = [library newFunctionWithName:@"SDFTextureFragmentMain"];
        assert(desc.vertexFunction != nil);
        assert(desc.fragmentFunction != nil);
        
        MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
        vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.attributes[0].offset = 0;

        vertexDescriptor.attributes[1].format = MTLVertexFormatFloat2;
        vertexDescriptor.attributes[1].bufferIndex = 0;
        vertexDescriptor.attributes[1].offset = sizeof(float) * 4;
        
        vertexDescriptor.attributes[2].format = MTLVertexFormatFloat2;
        vertexDescriptor.attributes[2].bufferIndex = 0;
        vertexDescriptor.attributes[2].offset = sizeof(float) * 4 + sizeof(float) * 2;
        
        vertexDescriptor.attributes[3].format = MTLVertexFormatFloat4;
        vertexDescriptor.attributes[3].bufferIndex = 0;
        vertexDescriptor.attributes[3].offset = sizeof(float) * 4 + sizeof(float) * 2 + sizeof(float) * 2;
        
        vertexDescriptor.layouts[0].stride = sizeof(float) * 4 + sizeof(float) * 2 + sizeof(float) * 2 + sizeof(float) * 4;
        vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
    
        desc.vertexDescriptor = vertexDescriptor;
        desc.sampleCount = 1;
        [self premulBlend:desc.colorAttachments[0]];

        TRMetalShader *shader = [self getShader:ShaderType_SDF];
        shader.pipeline = [_device newRenderPipelineStateWithDescriptor:desc error:&error];
        assert(shader.pipeline);
        
        desc = nil;
    }

     { // Single pipeline
        MTLRenderPipelineDescriptor *desc = [MTLRenderPipelineDescriptor new];
        
        desc.vertexFunction = [library newFunctionWithName:@"SingleVertexMain"];
        desc.fragmentFunction = [library newFunctionWithName:@"SingleFragmentMain"];
        assert(desc.vertexFunction != nil);
        assert(desc.fragmentFunction != nil);
        
        MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
        vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.attributes[0].offset = 0;

        vertexDescriptor.attributes[1].format = MTLVertexFormatFloat4;
        vertexDescriptor.attributes[1].bufferIndex = 0;
        vertexDescriptor.attributes[1].offset = sizeof(float) * 4;
        
        vertexDescriptor.layouts[0].stride = sizeof(float) * 4 + sizeof(float) * 4;
        vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
    
        desc.vertexDescriptor = vertexDescriptor;
        desc.sampleCount = 1;

        [self standardColorAttachment:desc.colorAttachments[0]]; 
        
        TRMetalShader *shader = [self getShader:ShaderType_Single];
        shader.pipeline = [_device newRenderPipelineStateWithDescriptor:desc error:&error];
        assert(shader.pipeline);
    }

    { // Texture Combiner
        MTLRenderPipelineDescriptor *desc = [MTLRenderPipelineDescriptor new];
        
        desc.vertexFunction = [library newFunctionWithName:@"TextureCombinerVertexMain"];
        desc.fragmentFunction = [library newFunctionWithName:@"TextureCombinerFragmentMain"];
        assert(desc.vertexFunction != nil);
        assert(desc.fragmentFunction != nil);
        
        MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
        vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
        vertexDescriptor.attributes[0].bufferIndex = 0;
        vertexDescriptor.attributes[0].offset = 0;

        vertexDescriptor.attributes[1].format = MTLVertexFormatFloat4;
        vertexDescriptor.attributes[1].bufferIndex = 0;
        vertexDescriptor.attributes[1].offset = sizeof(float) * 4;
        
        vertexDescriptor.layouts[0].stride = sizeof(float) * 4 + sizeof(float) * 4;
        vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
    
        desc.vertexDescriptor = vertexDescriptor;
        // desc.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float;
        // desc.stencilAttachmentPixelFormat = MTLPixelFormatStencil8;
        desc.sampleCount = 1;
        [self standardColorAttachment:desc.colorAttachments[0]];

        TRMetalShader *shader = [self getShader:ShaderType_TextureCombiner];
        shader.pipeline = [_device newRenderPipelineStateWithDescriptor:desc error:&error];
        assert(shader.pipeline);
    }
}

- (void)reloadShaders
{
    [self createShaders];
};

- (void)updateViewSize:(v2i)newSize
{
    v2i currentDrawSize = V2iFromCGSize(_metalLayer.drawableSize);
    if (newSize != currentDrawSize) {
        // @todo: The textures are not needed in FULL window size, just the rendersize.
        _metalLayer.drawableSize = CGSizeMake(newSize.x, newSize.y);
        _backbufferTexture = TextureInfo(0, newSize);
        [self createStaticTextures];
    }
}

- (void)setViewPort:(id<MTLRenderCommandEncoder>)encoder size:(v2i)size
{
    MTLViewport viewport;
    viewport.width = size.x;
    viewport.height = size.y;
    
    viewport.originX = 0;
    viewport.originY = 0;
    viewport.zfar = 1.0f;
    viewport.znear = 0.0f;
    [encoder setViewport:viewport];
}

- (void)runBasicPipes:(render_commands *)commands commandBuffer:(id<MTLCommandBuffer>)commandBuffer drawableTexture:(id<MTLTexture>)drawableTexture
{
#define WORKING_THING
#ifdef WORKING_THING
    // If we render directly to the drawableTexture, it looks fine.
    id<MTLTexture> renderTarget = drawableTexture;
#else
    // But, if we render to the renderTarget and blit that to the screen, it does not work?
    id<MTLTexture> renderTarget = [self getTexture:_renderTexture.renderHandle];
#endif

    v2i renderSize = _renderTexture.size;

    if(commands->indexCount > 0) { // Render primitives
        MTLRenderPassDescriptor *descriptor = [MTLRenderPassDescriptor renderPassDescriptor];
        descriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
        descriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.0f, 0.0f, 0.0f, 1.0f);
        descriptor.colorAttachments[0].storeAction = MTLStoreActionMultisampleResolve;
        descriptor.colorAttachments[0].resolveTexture = renderTarget;
        descriptor.colorAttachments[0].texture = [self getTexture:_msaaTexture.renderHandle];
     
        id<MTLRenderCommandEncoder> encoder = [commandBuffer renderCommandEncoderWithDescriptor:descriptor];
        [encoder pushDebugGroup:@"Primitives Renderpass"];
        [self setViewPort:encoder size:renderSize];

        // Draw primitives
        id<MTLBuffer> vertexBuffer = _simpleVertexBuffer[_flightBufferIndex];
        id<MTLBuffer> indexBuffer = _simpleIndexBuffer[_flightBufferIndex];
        
        memcpy(vertexBuffer.contents, commands->vertexArray, sizeof(simple_vertex) * commands->vertexCount);
        memcpy(indexBuffer.contents, commands->indexArray,   sizeof(u16) * commands->indexCount);
        
        [encoder setRenderPipelineState:[self getShader:ShaderType_Simple].pipeline];
        
        [encoder setVertexBuffer:vertexBuffer offset:0 atIndex:0];
        [encoder setVertexBytes:&commands->viewProjectionMatrix length:sizeof(m4x4) atIndex:1];

        [encoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
                                indexCount:commands->indexCount
                                indexType:MTLIndexTypeUInt16
                                indexBuffer:indexBuffer
                                indexBufferOffset:0];
        [encoder endEncoding];
    }

    {   // Render SDF lines
        MTLRenderPassDescriptor *descriptor = [MTLRenderPassDescriptor renderPassDescriptor];
        descriptor.colorAttachments[0].texture = renderTarget;
        descriptor.colorAttachments[0].loadAction = MTLLoadActionLoad;
        descriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.4f, 0, 0, 1);
        descriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
      
        id<MTLRenderCommandEncoder> encoder = [commandBuffer renderCommandEncoderWithDescriptor:descriptor];
        [encoder pushDebugGroup:@"SDF Renderpass"];
        [self setViewPort:encoder size:renderSize];

        if(commands->sdfIndexCount > 0) {
            id<MTLBuffer> sdfVertexBuffer = _sdfVertexBuffer[_flightBufferIndex];
            id<MTLBuffer> sdfIndexBuffer = _sdfIndexBuffer[_flightBufferIndex];
            
            memcpy(sdfVertexBuffer.contents, commands->sdfVertexArray, sizeof(sdf_vertex)  * commands->sdfVertexCount);
            memcpy(sdfIndexBuffer.contents, commands->sdfIndexArray,   sizeof(u16) * commands->sdfIndexCount);
            
            [encoder setRenderPipelineState:[self getShader:ShaderType_SDF].pipeline];
            
            [encoder setVertexBuffer:sdfVertexBuffer offset:0 atIndex:0];
            [encoder setVertexBytes:&commands->viewProjectionMatrix length:sizeof(m4x4) atIndex:1];

            [encoder setFragmentBytes:&commands->timer length:sizeof(float) atIndex:1];
            [encoder setFragmentBytes:&renderSize length:sizeof(v2i) atIndex:2];
    
            [encoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
                                    indexCount:commands->sdfIndexCount
                                    indexType:MTLIndexTypeUInt16
                                    indexBuffer:sdfIndexBuffer
                                    indexBufferOffset:0];
        }
    
        [encoder endEncoding];
    }
    

#ifndef WORKING_THING
    // Put the renderTarget onto the drawableTexture, either through a shader or blitting - doesn't matter.
#if 0
    {   // Blit
        id<MTLBlitCommandEncoder> blitCommandEncoder = [commandBuffer blitCommandEncoder];
        [blitCommandEncoder copyFromTexture:renderTarget
                            sourceSlice:0 sourceLevel:0
                            sourceOrigin:MTLOriginMake(0,0,0) sourceSize:MTLSizeMake(renderSize.x, renderSize.y, 1)
                            toTexture:drawableTexture destinationSlice:0 destinationLevel:0
                            destinationOrigin:MTLOriginMake(0,0,0)];
        [blitCommandEncoder endEncoding];
    }
#else
    {   // Draw renderTarget to backbuffer.
        MTLRenderPassDescriptor *descriptor = [MTLRenderPassDescriptor renderPassDescriptor];
        descriptor.colorAttachments[0].texture = drawableTexture;
        descriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
        descriptor.colorAttachments[0].clearColor = MTLClearColorMake(0, 0, 0, 1.0f);
        descriptor.colorAttachments[0].storeAction = MTLStoreActionStore;

        id<MTLRenderCommandEncoder> encoder = [commandBuffer renderCommandEncoderWithDescriptor:descriptor];
        [encoder pushDebugGroup:@"Combiner Renderpass"];
        [self setViewPort:encoder size:renderSize];

        [encoder setRenderPipelineState:[self getShader:ShaderType_RGBATexture].pipeline];
        
        f32 vertexArray[] = {
            -1.0f, -1.0f, 0.0f, 1.0f,  1.0f, 1.0f, 1.0f, 1.0f,
             1.0f, -1.0f, 1.0f, 1.0f,  1.0f, 1.0f, 1.0f, 1.0f,
             1.0f,  1.0f, 1.0f, 0.0f,  1.0f, 1.0f, 1.0f, 1.0f,

            -1.0f, -1.0f, 0.0f, 1.0f,  1.0f, 1.0f, 1.0f, 1.0f,
             1.0f,  1.0f, 1.0f, 0.0f,  1.0f, 1.0f, 1.0f, 1.0f,
            -1.0f,  1.0f, 0.0f, 0.0f,  1.0f, 1.0f, 1.0f, 1.0f,
        };

        [encoder setVertexBytes:vertexArray length:sizeof(f32) * ArrayCount(vertexArray) atIndex:0];
        [encoder setFragmentTexture:renderTarget atIndex:0];
        [encoder setFragmentSamplerState:_textureSamplerLinear atIndex:1];
        [encoder drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:6];
        [encoder endEncoding];
    }
#endif
#endif
}

- (void)outputRenderCommands:(render_commands *)commands
{
@autoreleasepool {
    
    dispatch_semaphore_wait(_inFlightSemaphore, DISPATCH_TIME_FOREVER);
    _flightBufferIndex = (_flightBufferIndex + 1) % GlobalMaxBuffersInFlight;

    id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];

    __block dispatch_semaphore_t blockSemaphore = _inFlightSemaphore;
    [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> buffer) {
         dispatch_semaphore_signal(blockSemaphore);
    }];

    // @todo: profiling.

    assert(_metalLayer != nil);
    id<CAMetalDrawable> drawable = [_metalLayer nextDrawable];
    assert(drawable != nil);
    assert(drawable.texture != nil);
    
    if(drawable) {
        [self runBasicPipes:commands commandBuffer:commandBuffer drawableTexture:drawable.texture];
        [commandBuffer presentDrawable:drawable];
        [commandBuffer commit];
    }
    
    ClearFrame(commands);
    
} // @autoreleasepool
}

@end // MetalRenderer
