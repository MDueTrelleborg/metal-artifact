vertex VertOutputF4F4F2
RGBATextureVertexMain(VertInputF4F4 input [[stage_in]])
{
    VertOutputF4F4F2 result;

    result.position = V4(input.position.x, input.position.y, 0.0, 1.0);
    result.color = input.color;
    result.uv = input.position.zw;
    
    return result;
}

fragment v4
RGBATextureFragmentMain(VertOutputF4F4F2 input [[stage_in]],
                    texture2d<float> texture   [[texture(0)]],
                    sampler textureSampler     [[sampler(1)]])
{
    v4 sample = texture.sample(textureSampler, input.uv);
    return sample;
}
