#ifndef INCLUDE_METAL_RENDERER_HEADER
#define INCLUDE_METAL_RENDERER_HEADER

global const NSInteger GlobalMaxBuffersInFlight = 3;
global b32 shadersAllocated = false;

enum shader_type
{
    ShaderType_Simple,
    ShaderType_Texture,
    ShaderType_RGBATexture,
    ShaderType_SDF,
    ShaderType_Single,
    ShaderType_TextureCombiner,
};

@interface TRMetalShader : NSObject
@property (nonatomic) shader_type type;
@property (nonatomic) string filePath;
@property (nonatomic) id<MTLRenderPipelineState> pipeline;
@end

@implementation TRMetalShader
@end

enum class TextureType {
    STANDARD,
    MSAA,
    STENCIL,
    DEPTH,
    DEPTH_MSAA,
    OFFSCREEN_TARGET,
    RENDER_TARGET,
};

enum pixel_format
{
    PixelFormat_UNKOWN,
    PixelFormat_RGBA,
    PixelFormat_BGRA,
};

@interface MetalRenderer : NSObject
{
    // @todo: The offscreen texture was blocked for some reason when switching render-paths. 
    // and I don't know why. So this is a work-around to have offscreen textures corresponding
    // to the different draw paths.
    @public texture_info _offscreenTextures[3];
    @public texture_info *_currentOffscreenTexture;
    @public texture_info _backbufferTexture;
}
- (instancetype)init:(CAMetalLayer *)metalLayer desiredAspectRatio:(f32)desiredAspectRatio;
- (texture_info)createTexture:(u8 *)data size:(v2i)size bytesPerPixel:(u32)bytesPerPixel format:(pixel_format)format;
@end

#endif // INCLUDE_METAL_RENDERER_HEADER
