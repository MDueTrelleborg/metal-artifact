vertex VertOutputF4F4F2
TextureCombinerVertexMain(VertInputF4F4 input [[stage_in]])
{
    VertOutputF4F4F2 result;
    
    result.position = V4(input.position.xy, 0.0, 1.0);
    result.color = input.color;
    result.uv = input.position.zw;

    return result;
}

fragment float4
TextureCombinerFragmentMain(VertOutputF4F4F2 input [[stage_in]],
                   texture2d<float> texture0       [[texture(0)]],
                   texture2d<float> texture1       [[texture(1)]],
                   sampler textureSampler          [[sampler(2)]])
{
    v4 sample0 = texture0.sample(textureSampler, input.uv);  
    v4 sample1 = texture1.sample(textureSampler, input.uv);  

    // @todo: Blend them together :) 
    v4 result = sample0 + sample1;
    return result;
}
